//
//  AddToDoViewController.swift
//  ToDoList
//
//  Created by epita on 17/05/2019.
//  Copyright © 2019 epita. All rights reserved.
//

import UIKit

class AddToDoViewController: UIViewController {
    var todoItem : ToDoItem?
    
    @IBOutlet weak var cancel: UIBarButtonItem!
    @IBOutlet weak var done: UIBarButtonItem!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let sender = sender as? UIBarButtonItem, sender == done {
            if let text = textField.text, text.count > 0 {
                todoItem = ToDoItem(itemName: text)
            }
        }
    }
}
