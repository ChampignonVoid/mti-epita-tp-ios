//
//  AddToDoViewController.swift
//  ToDoList
//
//  Created by epita on 17/05/2019.
//  Copyright © 2019 epita. All rights reserved.
//

import UIKit

class ToDoTableViewController: UITableViewController {
    @IBOutlet weak var addButtonItem: UIBarButtonItem!
    var toDoItems = [ToDoItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the number of rows
        return toDoItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
        
        if indexPath.row < toDoItems.count {
            let todoItem = toDoItems[indexPath.row]
            cell.textLabel?.text = todoItem.itemName
            cell.backgroundColor = todoItem.backgroundColor
            if todoItem.completed {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        return cell
    }
    

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            self.toDoItems.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        let update = UITableViewRowAction(style: .normal, title: "Update") { (action, indexPath) in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let currentItem = self.toDoItems[indexPath.item]
            let dateString = formatter.string(from: currentItem.creationDate)
            let message = "This item has been created at this date: \(dateString) and has " + (currentItem.completed ? "" : "not ") + "been completed"
            let alertController = UIAlertController(title: "Update ToDoItem", message: message, preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "Update", style: .default) { (_) in
                if let txtField = alertController.textFields?.first, let text = txtField.text {
                    currentItem.itemName = text
                    self.tableView.reloadRows(at: [indexPath], with: .top)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
            alertController.addTextField { (textField) in
                textField.text = currentItem.itemName
            }
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        update.backgroundColor = .cyan
        
        return [delete, update]
    }

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let movedObject = toDoItems[fromIndexPath.row]
        toDoItems.remove(at: fromIndexPath.row)
        toDoItems.insert(movedObject, at: to.row)
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    
    func loadInitialData(){
        let item1 = ToDoItem(itemName: "TODO 1")
        toDoItems.append(item1)
        let item2 = ToDoItem(itemName: "TODO 2")
        toDoItems.append(item2)
        let item3 = ToDoItem(itemName: "TODO 3")
        toDoItems.append(item3)
    }
    
    @IBAction func editClicked(_ sender: Any) {
        if let button = sender as? UIBarButtonItem {
            tableView.isEditing = !tableView.isEditing
            button.title = tableView.isEditing ? "Done" : "Edit"
            addButtonItem.isEnabled = !tableView.isEditing
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.row < toDoItems.count {
            let todoItem = toDoItems[indexPath.row]
            todoItem.completed = !todoItem.completed
            tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    @IBAction func unwindToList(_ segue:UIStoryboardSegue) {
        if let source = segue.source as? AddToDoViewController {
            if let item = source.todoItem {
                toDoItems.append(item)
                tableView.reloadData()
            }
        }
    }
}
