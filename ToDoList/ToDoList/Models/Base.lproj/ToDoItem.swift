//
//  ToDoItem.swift
//  ToDoList
//
//  Created by epita on 17/05/2019.
//  Copyright © 2019 epita. All rights reserved.
//

import UIKit

class ToDoItem {
    var itemName: String
    var completed: Bool
    var creationDate: Date
    var backgroundColor: UIColor
    
    init(itemName:String) {
        self.itemName = itemName
        completed = false
        creationDate = Date()
        backgroundColor = UIColor(red: .random(in: 0...1), green: .random(in: 0...1), blue: .random(in: 0...1), alpha: 1.0)
    }
}
